from typing import Optional, Dict, Tuple, Any

from utils import getsize


def veb_size(w):
    """
    Calculates size of VEB containing all w-bit numbers
    Suppose that one node of VEB requires no more than K bits of memory:
    for each root node we need to store: min, max and links to min, max and aux root node.

    Then size S(w) of VEB containing all w-bits integers is:
    S(w) = O(2^high * S(low) + S(high) + K), where low = w // 2 and high = w - low
    Because we need to store 2^high child VEBs containing low-bit integers and one aux VEB
    containing high-bit integers.
    Assuming S(0) <= K and S(1) <= K also we can move out K as multiplier and consider
    recursively defined function f, for which f(1) = 1, and f(w) = 2^high * f(low) + f(high) + 1.
    One can show that f(w) <= 2^w + 2^(w - 1) - 2.
    Because there at all 2^w w-bit integers, VEB contaning all of them requires O(2^w) space
    (including big hash table to store child nodes in VEBs
    that should contain O(f(w)) links to nodes).
    """
    if w <= 1:
        return 1
    else:
        high = w - w // 2
        low = w // 2
        return 1 + (2 ** high) * veb_size(low) + veb_size(high)


def split(x, bits):
    # return higher `bits / 2` bits and lower `bits / 2` bits as integers
    low_bits = bits // 2
    return x >> low_bits, x & ((1 << low_bits) - 1)


class Node:
    __slots__ = ('min', 'max', 'aux')

    aux: Optional['Node']

    def __init__(self):
        self.min = None
        self.max = None
        self.aux = None

    def __str__(self):
        return f'<Node #{id(self)} [{self.min}, {self.max}], aux: <{self.aux}>'

    def __repr__(self):
        return str(self)


class VEB:
    child_nodes: Dict[Tuple[int, int], Node]

    def __init__(self, bits: int):
        self.root = Node()
        self.child_nodes = dict()
        self.bits = bits

    def remove(self, x: int) -> bool:
        return _remove(self.root, x, self.bits, self.child_nodes)

    def insert(self, x: int) -> bool:
        return _insert(self.root, x, self.bits, self.child_nodes)

    def successor(self, x: int) -> Optional[int]:
        return _successor(self.root, x, self.bits, self.child_nodes)


def _remove(v: Node, x: Any, bits: int, child_nodes):
    if v.min is None:
        return False
    if x < v.min or v.max < x:
        return False
    if v.min == x and v.max == x:
        v.min = None
        v.max = None
        return True

    if v.min == x:
        # because minimum is not stored in subtrees we need to get a successor of x
        # as new minimum and remove this successor from its tree
        if v.aux is None:
            v.min = v.max
            return True
        # promote minimum from subtree
        subtree_index = v.aux.min
        subtree = child_nodes[id(v), subtree_index]
        element = subtree.min
        v.min = (subtree_index << bits // 2) + element
    elif v.max == x:
        if v.aux is None:
            v.max = v.min
            return True
        # promote new maximum for subtree
        subtree_index = v.aux.max
        subtree = child_nodes[(id(v), subtree_index)]
        element = subtree.max
        v.max = (subtree_index << bits // 2) + element
    elif v.aux is None:
        # v.min < x < v.max but tree does't have other child nodes
        return False
    else:
        subtree_index, element = split(x, bits)
        subtree = child_nodes.get((id(v), subtree_index))
        if subtree is None:
            return False

    res = _remove(subtree, element, bits // 2, child_nodes)

    # clean up tails
    if subtree.min is None:
        _remove(v.aux, subtree_index, bits - bits // 2, child_nodes)
        del child_nodes[id(v), subtree_index]
        if v.aux.min is None:
            v.aux = None

    return res


def _successor(v: Node, x: Any, bits, child_nodes):
    if v.min is None:
        return None

    if x < v.min:
        # min is the first greater element for x
        return v.min
    elif x >= v.max:
        # no greater element for x
        return None
    elif v.aux is None:
        # aux is None when tree contains one or two vertices
        return v.max
    else:

        high, low = split(x, bits)
        key = (id(v), high)
        if key in child_nodes and low < child_nodes[key].max:
            # tree is not empty and corresponding tree may contain elements between low and
            # v.cluster[high].max
            # because max is always stored in its tree it is safe to recursively call successor
            suc = _successor(child_nodes[key], low, bits // 2, child_nodes)
            return (high << (bits // 2)) + suc
        else:
            # high not in v.cluster or
            # low is the maximum of its high
            # anyway we need to check next nonempty tree
            next_tree = _successor(v.aux, high, bits - bits // 2, child_nodes)
            if next_tree is None:
                return v.max
            else:
                # merge next_tree index and its minimum
                return (next_tree << (bits // 2)) + child_nodes[(id(v), next_tree)].min


def _insert(v: Node, x: Any, bits: int, child_nodes):
    if v.min is None:
        # v.max is also None - inserting value to empty tree with root v
        v.min = x
        v.max = x
        return True
    elif v.min == x or v.max == x:
        return False
    elif v.min == v.max:
        # current root node v contains exactly one value
        if v.min < x:
            v.max = x
        elif v.min > x:
            v.min = x
        return True
    else:
        if v.min > x:
            x, v.min = v.min, x
        if v.max < x:
            # do not insert max into subtrees
            x, v.max = v.max, x
        if bits > 1:
            high, low = split(x, bits)
            # before recursive insert lazily create child node if it does not exist
            key = (id(v), high)
            if key not in child_nodes:
                child_node = Node()
                # lazily create aux tree for current node
                if v.aux is None:
                    v.aux = Node()
                # insert child's node `high` index into aux tree
                _insert(v.aux, high, bits - bits // 2, child_nodes)
                child_nodes[key] = child_node
            else:
                child_node = child_nodes[key]
            return _insert(child_node, low, bits // 2, child_nodes)
