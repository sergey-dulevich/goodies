class SegmentTree:
    def __init__(self, values, default, func):
        """
        Segment tree to find Value function for any segment of some array A.

        Suppose we want to find some V for every slice of a given array
        and that if we know values on slices [a:b] and [b:c] then it's easy to
        calculate value for slice [a:c] - in other words there exists associative binary
        operation ~ such that for any given a, b (where 0 <= a <= b < len(A))
            V(A[a:b]) = V(A[a:c]) ~ V(A[c:b]) for any c between a and b, particularly:
            V(A[a:b]) = V(A[a]) ~ V(A[a + 1]) ~ ... ~ V(A[b - 1])

        And suppose that we need to change separate values in array and keep results up to date.

        Example 1:
            ~ is min of two numbers and Value(array[a:b]) is minimum element on slice array[a:b]
        Example 2:
            ~ is addition and Value(array[a:b]) means sum of elements in A[a:b])

        Args:
            values:
                pre-calculated values for V function on elements of array
            default:
                value for empty segment, such that func(v, default) = v
            func:
                combining binary operation as function taking two arguments: func(a, b) means a ~ b
        """
        k = 0
        while 2 ** k < len(values):
            k += 1

        # heap_size = 2 ** (k + 1) - 1
        heap = (2 ** k - 1) * [default] + list(values) + (2 ** k - len(values)) * [default]
        for index in range(2 ** k - 1 - 1, -1, -1):
            heap[index] = func(heap[2 * index + 1], heap[2 * index + 2])

        self._length = len(values)
        self.func = func
        self.heap = heap
        self.k = k
        self.default = default

    def __getitem__(self, indexer):
        shift = 2 ** self.k - 1
        if isinstance(indexer, int):
            return self.heap[shift + indexer]
        elif isinstance(indexer, slice):
            left, right = shift + indexer.start, shift + indexer.stop - 1
            if left > right:
                return self.default
            func = self.func
            heap = self.heap
            res = func(heap[left], heap[right])
            while left < right - 1:
                if left & 1:
                    res = func(res, heap[left + 1])
                if ~(right & 1):
                    res = func(res, heap[right - 1])
                left = (left + 1) // 2 - 1
                right = (right + 1) // 2 - 1
            return res
        raise TypeError()

    def __setitem__(self, array_index, value):
        if array_index > len(self):
            raise IndexError
        index = (2 ** self.k) - 1 + array_index
        heap = self.heap
        func = self.func
        heap[index] = value
        while index:
            # until there is an ancestor node in heap for current index
            index = (index + 1) // 2 - 1
            heap[index] = func(heap[2 * index + 1], heap[2 * index + 2])

    def __len__(self):
        return self._length
